# common_variables.py
import pygame
from enum import Enum

class CommonVariables:
    def __init__(self):
        # Constants
        self.WindowWidth = 300
        self.WindowHeight = 400
        self.scale = 90
        self.offset = 45
        self.start_x = self.WindowWidth / 2 - self.scale
        self.start_y = self.WindowHeight / 2 - self.scale - self.offset
        self.dice_width = 225
        self.dice_length = 225
        # Use a dictionary to store the results
        self.result_p1 = {}
        self.result_p2 = {}
        self.result_overall = {}
        self.round = 0

        self.dice_surface = pygame.display.set_mode((self.WindowWidth, self.WindowHeight))
        self.clock = pygame.time.Clock()
        self.MY_VERSION = "1.07"

        # Dice Faces
        # One
        self.ON = ['...',
                   '.X.',
                   '...']
        # Two
        self.TW = ['X..',
                   '...',
                   '..X']
        # Three
        self.TH = ['X..',
                   '.X.',
                   '..X']
        # Four
        self.FO = ['X.X',
                   '...',
                   'X.X']

        # Five
        self.FI = ['X.X',
                   '.X.',
                   'X.X']

        # Six
        self.SI = ['X.X',
                   'X.X',
                   'X.X']

        # Map each face array to a single array
        self.dice_faces = [self.ON,
                            self.TW,
                            self.TH,
                            self.FO,
                            self.FI,
                            self.SI]

class Colour(Enum):
    BLACK = (10, 10, 10)
    WHITE = (255, 255, 255)
    BLUE = (22, 55, 200)
    RED = (200, 55, 10)
    GREEN = (0, 200, 0)
    LIGHT_GREEN = (101, 152, 101)
    GREY = (128, 128, 128)
