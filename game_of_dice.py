""" Version 1.07 - Simple program to display a Six Sided Dice.
The dice can be "thrown" and be used for simple games.
The current value that has been "thrown", is shown in the bottom left,
while the last 10 historical values are displayed along the bottom of the screen - with indicators
as to who won i.e. GREEN for Player 1 and BLUE for Player 2. 
Phil Jones October 2019 - phil.jones.24.4@gmail.com 
V1.06 Added historic scores and results to UI
V1.07 Toned down colours
V1.08 09/23 - Make UI Imporvements
"""

import pygame
import random
# Import the CommonVariables class from the common_variables module
from common_variables import CommonVariables
from common_variables import Colour as co
# Create an instance of the CommonVariables class
cv = CommonVariables()


def throw():
    """
    Generates a random dice roll value between 1-6.
    
    No parameters.
    
    Returns:
        int: A random integer between 1-6 inclusive, representing the dice roll value.
    """
    # Generate a number 1 - 6
    dice_throw = random.randrange(1, 7)
    return dice_throw


def update_score(round, go, current_throw):
    """
    Updates the score dictionaries with the current throw result for the player.
    
    Parameters:
        round (int): The current round number
        go (int): The current player number (1 or 2)
        current_throw (int): The dice value just thrown
    
    Returns:
        None
        
    This function updates the result dictionary for the current player with an entry 
    containing the round number and their throw value for that round.
    The entry is added to either the result_p1 or result_p2 dictionary depending on 
    the 'go' player number parameter.
    """
    if go == 1:
        d = {"Player 1 for round " + str(round): current_throw}
        cv.result_p1.update(dict(d))  # update it
    else:
        d2 = {"Player 2 for round " + str(round): current_throw}
        cv.result_p2.update(dict(d2))


def draw_dice(surface, throw, go):
    """
    Draws the visual representation of a dice roll onto the provided surface.
    
    Parameters:
        surface (pygame.Surface): The surface to draw the dice on 
        throw (int): The dice roll value (1-6)
        go (int): The current player number (1 or 2)
        
    Returns:
        None
    
    This function draws the dice face corresponding to the provided throw 
    value on the given surface. It draws a colored border based on the 
    current player, looks up the 2D dot pattern array for the throw value, 
    and draws white circles for each 'X' in the array. This results in the 
    correct dice face being drawn for the player's roll.
    """
    # Wipe Screen
    cv.dice_surface.fill(co.BLACK.value)
    # Draw Dice Border
    if go == 1:
        my_colour = co.GREEN.value
    else:
        my_colour = co.BLUE.value
    pygame.draw.line(surface, my_colour, (cv.start_x - cv.offset, cv.start_y - cv.offset), (cv.start_x + cv.dice_width, cv.start_y - cv.offset))
    pygame.draw.line(surface, my_colour, (cv.start_x - cv.offset, cv.start_y - cv.offset), (cv.start_x - cv.offset, cv.start_y + cv.dice_length))
    pygame.draw.line(surface, my_colour, (cv.start_x + cv.dice_width, cv.start_y - cv.offset),
                     (cv.start_x + cv.dice_width, cv.start_y + cv.dice_length))
    pygame.draw.line(surface, my_colour, (cv.start_x - cv.offset, cv.start_y + cv.dice_length),
                     (cv.start_x + cv.dice_width, cv.start_y + cv.dice_length))
    # Draw Dice Face Based on which 3x3 array has been selected
    for i in (range(3)):
        for j in (range(3)):
            # Nested loop is needed to iterate over the selected dice face array (3 x 3)
            if cv.dice_faces[throw - 1][j][i] == "X":
                pygame.draw.circle(surface, co.WHITE.value, (int(cv.start_x + cv.scale * i), int(cv.start_y + cv.scale * j)), int(cv.scale / 3))
    # Update the screen
    pygame.display.flip()


def draw_round_message(surface, font, round):
    """
    Draws the message for the start of a new round onto the given surface.
    
    Parameters:
        surface (pygame.Surface): The surface to draw the message on
        font (pygame.font.Font): The font to use 
        round (int): The current round number
        
    Returns: None
    
    This function draws the "Space to start" text prompt 
    centered on the screen to indicate the start of a new round.
    It fills the screen black, renders the message text in red,
    and blits it centered vertically and horizontally.
    """
    # Wipe Screen
    cv.dice_surface.fill(co.BLACK.value)
    text = font.render("SPACE TO START.....", True, co.RED.value)
    surface.blit(text, [cv.offset / 2, cv.WindowHeight / 2 - cv.offset])
    pygame.display.flip()


def draw_score(surface, font, current_throw, font2, round, go, cycle):
    """
    Draws the current and historical scores/results on the given surface.
    
    Parameters:
        surface (pygame.Surface): The surface to draw on
        font (pygame.font.Font): The font for main messages
        current_throw (int): The current dice roll value
        font2 (pygame.font.Font): The font for scores
        round (int): The current round number 
        go (int): The current player number
        cycle (int): The cycle within the round
        
    Returns:
        dict: The updated results dictionary
        
    This function displays the current player's roll message, 
    the historical scores/results for each player, indicators 
    for which player won each round, and the overall winner.
    
    It updates the results dictionary with the winner for the 
    current round.
    """
    # Get historical results
    pad = 4
    offset_px = 0
    if go == 1 and cycle < 3:
        intermediate_message = f"Player 1 has thrown a {current_throw}..."
        intermediate_message = font2.render(intermediate_message, True, co.GREEN.value)
        surface.blit(intermediate_message, [cv.start_x - cv.offset, cv.WindowHeight - cv.offset * 2])
        # Update the screen
        pygame.display.flip()
        return
    if go == 2 and cycle < 3:
        intermediate_message = f"Player 2 has thrown a {current_throw}..."
        intermediate_message = font2.render(intermediate_message, True, co.BLUE.value)
        surface.blit(intermediate_message, [cv.start_x - cv.offset, cv.WindowHeight - cv.offset * 2])
        # Update the screen
        pygame.display.flip()
        return
    if cycle == 3:
        pygame.draw.rect(surface, co.BLACK.value,(0, cv.WindowHeight - cv.offset * 2 - pad * 2, cv.WindowWidth, cv.WindowHeight // 2))
        pygame.display.flip()
        if len(cv.result_overall) > 0:
            for x in list(reversed(list(cv.result_overall)))[0:15]:
                win_indicator = "/"
                draw_indicator = "X"
                draw_indicator_blit = font2.render(draw_indicator, True, co.GREY.value)
                win_indicator_blit_p1 = font2.render(win_indicator, True, co.GREEN.value)
                win_indicator_blit_p2 = font2.render(win_indicator, True, co.BLUE.value)
                if format(cv.result_overall[x]) == "D":
                    surface.blit(draw_indicator_blit, [cv.start_x + offset_px + pad + 2, cv.WindowHeight - cv.offset * 2])
                    offset_px += 20
                if format(cv.result_overall[x]) == "P1":
                    surface.blit(win_indicator_blit_p1, [cv.start_x + offset_px + pad + 2, cv.WindowHeight - cv.offset * 2])
                    offset_px += 20
                if format(cv.result_overall[x]) == "P2":
                    surface.blit(win_indicator_blit_p2, [cv.start_x + offset_px + pad + 2, cv.WindowHeight - cv.offset * 2])
                    offset_px += 20
        # Get historical scores
        # Print the last 15 values from our p1 and p2 result dictionary
        offset_px = 0
        if len(cv.result_p1) > 0:
            for x in list(reversed(list(cv.result_p1)))[0:15]:
                # Draw dividing lines
                pygame.draw.line(surface, co.GREY.value, (cv.start_x + offset_px, cv.WindowHeight - (cv.offset * 2) - pad * 2),
                                (cv.start_x + offset_px, cv.WindowHeight - 5))
                history = font2.render(format(cv.result_p1[x]), True, co.GREEN.value)
                surface.blit(history, [cv.start_x + offset_px + pad, cv.WindowHeight - cv.offset * 1.5])
                offset_px += 20

        offset_px = 0
        if len(cv.result_p2) > 0:
            for x in list(reversed(list(cv.result_p2)))[0:15]:
                history = font2.render(format(cv.result_p2[x]), True, co.BLUE.value)
                surface.blit(history, [cv.start_x + offset_px + pad, cv.WindowHeight - cv.offset ^ 2 * 2])
                offset_px += 20

        # Extract the last winner
        p1_result = "0"
        p2_result = "0"
        for x in list(reversed(list(cv.result_p1)))[0:1]:
            p1_result = format(cv.result_p1[x])
        for x in list(reversed(list(cv.result_p2)))[0:1]:
            p2_result = format(cv.result_p2[x])


        # Calculate Winner Now
        won = "D"
        if p1_result > p2_result:
            text = font.render("P1", True, co.GREEN.value)
            text2 = font2.render("WON",True, co.GREEN.value)
            surface.blit(text2, [cv.offset / 64, cv.WindowHeight - 30])
            won = "P1"
        if p1_result < p2_result:
            text = font.render("P2", True, co.BLUE.value)
            text2 = font2.render("WON", True, co.BLUE.value)
            surface.blit(text2, [cv.offset / 64, cv.WindowHeight - 30])
            won = "P2"
        if p1_result == p2_result:
            text = font.render("D", True, co.WHITE.value)
            won = "D"
        surface.blit(text, [cv.offset / 64, cv.WindowHeight - 80])

        # Display the round number
        round_blit = font2.render("R: " + str(round), True, co.RED.value)
        surface.blit(round_blit, [cv.offset / 16, cv.WindowHeight - 100])

        # Update the screen
        pygame.display.flip()

        # Record overall round results and return dict (for future use)
        d = {"round " + str(round): won}
        cv.result_overall.update(dict(d))

        return (cv.result_overall)

def spin(surface, go):
    """
    Generates a spinning dice animation by rapidly drawing different faces.
    
    Parameters:
        surface (pygame.Surface): The surface to draw the dice on
        go (int): The current player number
        
    Returns: None
    
    This function quickly loops through drawing all 6 dice faces in succession
    to create a spinning dice animation effect. It draws each face around 200 
    times while looping from 1 to 6 to show the dice "spinning" before landing
    on the final rolled value.
    """
    # Generate a spin effect
    for i in range(200):
        for j in range(6):
            draw_dice(surface, j, go)


def main():
    """
    The main function that runs the game loop and handles events.
    
    No parameters.
    
    Returns: None
    
    This is the main game loop that initializes PyGame, 
    sets up fonts, runs the round loop, handles keyboard
    events, calls functions to draw the dice and scores
    each frame, and updates game state.
    
    Key logic includes:
    
    - Initialize PyGame and fonts
    - Enter main loop 
    - Draw current game state each frame
    - Check for Space key events to start new round
    - Call spin() and throw() when round starts
    - Increment rounds, players, cycles 
    - Update scores and winner each round
    - Loop back to redraw and check events
    
    The main loop orchestrates the overall game flow and visuals.
    Actual game logic is handled in separate functions.
    """
    loop = True
    # Declare Global Vars

    start = False
    round = 1
    go = 0
    go_per_round = 2
    cycle = 0
    display_score_cycle = 3

    pygame.init()
    pygame.display.set_caption("Dice " + cv.MY_VERSION)

    pygame.key.set_repeat(10, 500)

    # Initialise fonts we will use
    font = pygame.font.SysFont('consolas', 50, False, False)
    font2 = pygame.font.SysFont('consolas', 20, False, False)

    current_throw = throw()

    # Ensure space can not be pressed multiple times
    pygame.key.set_repeat(0, 500)

    while loop:

        # Control FPS
        cv.clock.tick(20)

        # Draw Dice
        if start and not cycle == display_score_cycle:
            draw_dice(cv.dice_surface, current_throw, go)

        # Update Score
        if start and not cycle == display_score_cycle:
            update_score(round, go, current_throw)

        # Draw Score
        if start:
            draw_score(cv.dice_surface, font, current_throw, font2, round, go, cycle)

        # Round start
        if not start:
            draw_round_message(cv.dice_surface, font2, round)


        # Handle quit
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE and pygame.KEYUP:
                    go += 1
                    cycle += 1
                    start = True
                    if cycle > display_score_cycle:
                        cycle = 1
                        if go > go_per_round:
                            round += 1
                            go = 1

                    if cycle == 1 or cycle == 2:
                        spin(cv.dice_surface, go)
                        current_throw = throw()


# Call main
main()
